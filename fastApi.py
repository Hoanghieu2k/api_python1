import mysql.connector
from fastapi import FastAPI
from pydantic import BaseModel
# create connection to MySQL
data = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="python_database"
)


class user():
    def __init__(self, Name, Pass):
        self.Name = Name
        self.Pass = Pass


# # Potsman
app = FastAPI()
data_global = my_cursor = data.cursor()


class MyItem(BaseModel):
    Name: str
    Pass: str


def convert_list_to_dict(i):
    dict_user = {
        "Name": i.Name,
        "Pass": i.Pass,
    }
    return dict_user


@app.get("/get_api")
async def get_api():
    list_result = []
    list_user = []
    data_global
    my_sql_insert = "SELECT * FROM `username`"
    my_cursor.execute(my_sql_insert)
    my_result = my_cursor.fetchall()
    my_cursor.close()

    for x in my_result:
        list_user.append(user(x[1], x[2]))
    for i in list_user:
        dict_user = convert_list_to_dict(i)
        list_result.append(dict_user)
    return list_result

# Post Api


@app.post("/post_api")
async def post_api(item: MyItem):
    data_global
    my_sql_insert = "INSERT INTO `username` (`Id`, `Name`, `Pass`) VALUES (NULL, '%s', '%s')" % (
        item.Name, item.Pass)
    my_cursor.execute(my_sql_insert)
    data.commit()
    return (item.Name, item.Pass)

# delete Api


@app.delete("/delete_api/{id}")
async def delete_api(id: int):
    data_global
    my_sql_insert = "DELETE FROM `username` WHERE `Id` = %s" % id
    my_cursor.execute(my_sql_insert)
    data.commit()
    return("Xoá thành công")

# Update


@app.put("/update_api/{id}")
async def update_api(id: int, item: MyItem):
    data_global
    my_sql_insert = "UPDATE `username` SET  `Name` = '%s', `Pass` = '%s' WHERE `username`.`Id` = %s" % (
        item.Name, item.Pass, id)
    my_cursor.execute(my_sql_insert)
    data.commit()
    return ("Sửa thành công")
