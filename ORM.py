# Orm
from fastapi import FastAPI
from pydantic import BaseModel
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

Base = declarative_base()


class User (Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    username = Column(String(50))
    password = Column(String(50))


class User (Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    username = Column(String(50))
    password = Column(String(50))


engine = create_engine('mysql://root:@localhost/python_database', echo=True)
Base.metadata.create_all(bind=engine)
Session = sessionmaker(bind=engine)

Session_new = Session()

'''API'''


app = FastAPI()


class MyItem(BaseModel):
    username: str
    password: str


class user_new():
    def __init__(self, username, password):
        self.username = username
        self.password = password


def convert_list_to_dict(i):
    dict_user = {
        "username": i.username,
        "password": i.password,
    }
    return dict_user


'''Get'''


@app.get("/get_api")
async def get_api():
    list_result = []
    list_user = []
    result = Session_new.query(User).all()
    for x in result:
        list_user.append(user_new(x.username, x.password))
    for i in list_user:
        dict_user = convert_list_to_dict(i)
        list_result.append(dict_user)
    return list_result

'''Post'''


@app.post("/post_api")
async def post_api(item: MyItem):
    User1 = User(username=item.username, password=item.password)
    Session_new.add(User1)
    Session_new.commit()
    return (item.username, item.password)

'''Delete'''


@app.delete("/delete_api/{id}")
async def delete_api(id: str):
    delete_user = Session_new.query(User).filter(User.id == id).all()
    Session_new.delete(delete_user)
    Session_new.commit()
    return ("Xoa thanh cong")

'''Update'''


@app.put("/update_api/{id}")
async def update_api(id: int, item: MyItem):
    update_user = Session_new.query(User).filter(User.id == id).first()
    update_user.username = item.username
    update_user.password = item.password
    Session_new.commit()
    return ("Sửa thành công")


'''Thêm người dùng'''
# User1 = User(username="", password="Hieu@2123")
# User2 = User(username="Thành1", password="Hieu@122342")
# User3 = User(username="Trường1", password="Hieu@122344")
# Session_new.add_all([User1, User2, User3])
# Session_new.commit()
# print(Session_new)
'''Xoá người dùng'''
# delete_user = Session_new.query(User).filter(User.id == '23').first()
# Session_new.delete(delete_user)
# Session_new.commit()
'''In ra danh sách người dùng'''
# Users = Session_new.query(User).all()
# for i in Users:
#     print("User new with id :%d, username :%s,password :%s" %
#           (i.id, i.username, i.password))
'''Sửa danh sách người dùng'''
# update_user = Session_new.query(User).filter(User.username == 'Hiếu4').first()
# update_user.username = 'Kakarot'
# # Session_new.put(update_user)
# Session_new.commit(update_user)
